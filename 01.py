#!/usr/bin/env python
# coding: utf-8

def reverse(s):
    """
    文字列を逆順にして返します.

    >>> reverse('stressed')
    'desserts'
    """
    return s[::-1]


tgt = "stressed"
reverse(tgt)


if __name__=="__main__":
    import doctest
    doctest.testmod()






